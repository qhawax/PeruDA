# To generate a project
https://start.spring.io/

- [Microservices tutorial](https://github.com/koushikkothagal/spring-boot-microservices-workshop)
- [@Value tutorial: variables, Lists, Hashes](https://www.youtube.com/watch?v=NFQDqEhx2e0&list=PLqq-6Pq4lTTaoaVoQVfRJPqvNTCjcTvJB&index=5)
- [Configuring many variables and map them into a class (@Configuration (Bean), @ConfigurationProperties)](https://www.youtube.com/watch?v=z8kfFbfGGME&list=PLqq-6Pq4lTTaoaVoQVfRJPqvNTCjcTvJB&index=6&t=208)
- [Actuators](https://www.youtube.com/watch?v=z8kfFbfGGME&list=PLqq-6Pq4lTTaoaVoQVfRJPqvNTCjcTvJB&index=6&t=434)
- [Exposing some variables](https://localhost:8080/actuator/configprops)
- Controlling what to expose **ConfigurationProperties**:
management.endpoints.web.exposure.include=*
- [YAML](https://www.youtube.com/watch?v=RUYV4P68hiE&list=PLqq-6Pq4lTTaoaVoQVfRJPqvNTCjcTvJB&index=7)
    - [X] - [Final Example](https://www.youtube.com/watch?v=RUYV4P68hiE&list=PLqq-6Pq4lTTaoaVoQVfRJPqvNTCjcTvJB&index=7&t=335)
    - [Externalize properties](https://www.youtube.com/watch?v=P91tqdWUHE4&list=PLqq-6Pq4lTTaoaVoQVfRJPqvNTCjcTvJB&index=8&t=35) ... 
    See [application-\<profileName\>.extn](https://www.youtube.com/watch?v=P91tqdWUHE4&list=PLqq-6Pq4lTTaoaVoQVfRJPqvNTCjcTvJB&index=8&t=300)
    - in application.yml add:
        - [spring.profiles.active: test](https://www.youtube.com/watch?v=P91tqdWUHE4&list=PLqq-6Pq4lTTaoaVoQVfRJPqvNTCjcTvJB&index=8&t=384)
    - [Multiple environments](https://www.youtube.com/watch?v=P91tqdWUHE4&list=PLqq-6Pq4lTTaoaVoQVfRJPqvNTCjcTvJB&index=8&t=496)
    - [Changing configuration on command line](https://www.youtube.com/watch?v=P91tqdWUHE4&list=PLqq-6Pq4lTTaoaVoQVfRJPqvNTCjcTvJB&index=8&t=725)
    - [Selecting beans by profile](https://www.youtube.com/watch?v=P91tqdWUHE4&list=PLqq-6Pq4lTTaoaVoQVfRJPqvNTCjcTvJB&index=8&t=805)
- [Spring Cloud Config Server](https://www.youtube.com/watch?v=JSdy9Q8Uk34&list=PLqq-6Pq4lTTaoaVoQVfRJPqvNTCjcTvJB&index=10)
    - [What is the URL? (explanation)](https://www.youtube.com/watch?v=JSdy9Q8Uk34&list=PLqq-6Pq4lTTaoaVoQVfRJPqvNTCjcTvJB&index=11&t535)
    - [What is the URL?](http://localhost:8888/application/default) application.yml is where I have my variables
    - [Fixing bugs for centralized configuration](https://codeburst.io/spring-cloud-config-centralized-configuration-in-microservices-f4ec243512db)
- [Spring Cloud Configuration Client](https://www.youtube.com/watch?v=E2HkL766VHs&list=PLqq-6Pq4lTTaoaVoQVfRJPqvNTCjcTvJB&index=12)
- [How to update variables in realtime?](https://www.youtube.com/watch?v=yNnLICy2zk4&list=PLqq-6Pq4lTTaoaVoQVfRJPqvNTCjcTvJB&index=13&t=260)
- 
    1. [First: include actuator into your project]
    2. Add @RefreshScope
