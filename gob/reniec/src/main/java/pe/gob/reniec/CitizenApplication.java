package pe.gob.reniec;

import org.springframework.boot.SpringApplication;
import pe.gob.general.microservice.annotation.PDAApplication;

@PDAApplication
public class CitizenApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(CitizenApplication.class, args);
    }

}
