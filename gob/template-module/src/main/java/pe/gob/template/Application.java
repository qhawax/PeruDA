package pe.gob.template;

import org.springframework.boot.SpringApplication;
import pe.gob.general.microservice.annotation.PDAApplication;

@PDAApplication
public class Application
{
    public static void main(String[] args)
    {
        SpringApplication.run(Application.class, args);
    }

}
