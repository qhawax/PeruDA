package pe.gob.template;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.gob.general.microservice.BaseController;

@RestController
public class Microservice extends BaseController
{
  @RequestMapping("/prueba2")
  String test() {
    return "prueba";
  }
}
