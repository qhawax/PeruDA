package pe.gob.general.repository;

import pe.gob.general.entity.LogQuery;

public interface LogQueryRepository extends PDARepository<LogQuery, String>
{
}

