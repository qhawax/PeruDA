package pe.gob.general.repository;

import org.springframework.data.mongodb.repository.Query;
import pe.gob.general.entity.ClickLog;

import java.util.List;

public interface ClickLogRepository extends PDARepository<ClickLog, String>
{
    @Query("timestamp.year")
    List<ClickLog> findByYear(int year);

    @Query("{timestamp.month:?month, timestamp.day:?day,}")
    List<ClickLog> findByMonthAndDay(Integer month, Integer day);

    @Query("{application:?0, module:?1, timestamp.month:?2, timestamp.day:?3,}")
    List<ClickLog> findByApplicationAndModuleAndMonthAndDay(String application, String Module, Integer month, Integer day);


}
