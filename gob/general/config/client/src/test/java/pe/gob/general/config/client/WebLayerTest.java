package pe.gob.general.config.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pe.gob.general.config.client.ClientController;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ClientController.class)
@AutoConfigureRestDocs(outputDir = "build/snippets")
public class WebLayerTest {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private ObjectMapper objectMapper;

  @Test
  public void shouldReturnDefaultMessage() throws Exception {
    this.mockMvc.perform(get("/test")).andDo(print()).andExpect(status().isOk())
            .andExpect(content().string(containsString("Greeting")))
            .andDo(document("home"));
  }
  @Test
  public void testPost() throws Exception {
    Map<String, Object> requestBody = new HashMap<>();
    requestBody.put("key1", 1);
    requestBody.put("key2", "value2");
    byte[] content = objectMapper.writeValueAsBytes(requestBody);
    this.mockMvc.perform(post("/test2").contentType(MediaType.APPLICATION_JSON).content(content))
            .andDo(print()).andExpect(status().isAccepted())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.key1", is(1)))
            .andExpect(jsonPath("$.key2", is("value2")))
            .andDo(document("home2"));
  }
}